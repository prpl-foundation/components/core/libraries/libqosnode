/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <qoscommon/qos-shaper.h>
#include "qos-node-api.h"
#include "qos-node.h"
#include "qos-assert.h"

static inline int qos_node_is_shaper(const qos_node_t* node) {
    int retval = 0;

    when_null(node, exit);
    retval = (node->type == QOS_NODE_TYPE_SHAPER);
exit:
    return retval;
}

bool qos_node_shaper_get_enable(const qos_node_t* node) {
    bool enable = false;

    when_null(node, exit);
    when_false(qos_node_is_shaper(node), exit);

    enable = qos_shaper_dm_get_enable(node->data.shaper);
exit:
    return enable;
}

qos_status_t qos_node_shaper_get_status(const qos_node_t* node) {
    qos_status_t status = QOS_STATUS_LAST;

    when_null(node, exit);
    when_false(qos_node_is_shaper(node), exit);

    status = qos_shaper_dm_get_status(node->data.shaper);
exit:
    return status;
}

const char* qos_node_shaper_get_interface(const qos_node_t* node) {
    const char* interface = NULL;

    when_null(node, exit);
    when_false(qos_node_is_shaper(node), exit);

    interface = qos_shaper_get_interface_name(node->data.shaper);
exit:
    return interface;
}

int32_t qos_node_shaper_get_shaping_rate(const qos_node_t* node) {
    int32_t shaping_rate = -1;

    when_null(node, exit);
    when_false(qos_node_is_shaper(node), exit);

    shaping_rate = qos_shaper_dm_get_shaping_rate(node->data.shaper);
exit:
    return shaping_rate;
}

uint32_t qos_node_shaper_get_shaping_burst_size(const qos_node_t* node) {
    uint32_t shaping_burst_size = 0;

    when_null(node, exit);
    when_false(qos_node_is_shaper(node), exit);

    shaping_burst_size = qos_shaper_dm_get_shaping_burst_size(node->data.shaper);
exit:
    return shaping_burst_size;
}

uint32_t qos_node_shaper_get_index(const qos_node_t* node) {
    uint32_t index = 0;

    when_null(node, exit);
    when_false(qos_node_is_shaper(node), exit);

    index = qos_shaper_dm_get_index(node->data.shaper);
exit:
    return index;
}