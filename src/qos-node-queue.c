/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <qoscommon/qos-queue.h>
#include "qos-node-api.h"
#include "qos-node.h"
#include "qos-assert.h"

static inline int qos_node_is_queue(const qos_node_t* node) {
    int retval = 0;

    when_null(node, exit);
    retval = (node->type == QOS_NODE_TYPE_QUEUE);
exit:
    return retval;
}

bool qos_node_queue_get_enable(const qos_node_t* node) {
    bool enable = false;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    enable = qos_queue_dm_get_enable(node->data.queue);
exit:
    return enable;
}

qos_status_t qos_node_queue_get_status(const qos_node_t* node) {
    qos_status_t status = QOS_STATUS_LAST;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    status = qos_queue_dm_get_status(node->data.queue);
exit:
    return status;
}

const char* qos_node_queue_get_alias(const qos_node_t* node) {
    const char* alias = NULL;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    alias = qos_queue_dm_get_alias(node->data.queue);
exit:
    return alias;
}

const char* qos_node_queue_get_traffic_classes(const qos_node_t* node) {
    const char* traffic_classes = NULL;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    traffic_classes = qos_queue_dm_get_traffic_classes(node->data.queue);
exit:
    return traffic_classes;
}

const char* qos_node_queue_get_interface(const qos_node_t* node) {
    const char* interface = NULL;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    interface = qos_queue_get_interface_name(node->data.queue);
exit:
    return interface;
}

bool qos_node_queue_get_all_interfaces(const qos_node_t* node) {
    bool all_interfaces = false;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    all_interfaces = qos_queue_dm_get_all_interfaces(node->data.queue);
exit:
    return all_interfaces;
}

bool qos_node_queue_get_hardware_assisted(const qos_node_t* node) {
    bool hardware_assisted = false;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    hardware_assisted = qos_queue_dm_get_hardware_assisted(node->data.queue);
exit:
    return hardware_assisted;
}

uint32_t qos_node_queue_get_buffer_length(const qos_node_t* node) {
    uint32_t buffer_length = 0;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    buffer_length = qos_queue_dm_get_buffer_length(node->data.queue);
exit:
    return buffer_length;
}

uint32_t qos_node_queue_get_weight(const qos_node_t* node) {
    uint32_t weight = 0;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    weight = qos_queue_dm_get_weight(node->data.queue);
exit:
    return weight;
}

uint32_t qos_node_queue_get_precedence(const qos_node_t* node) {
    uint32_t precedence = 0;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    precedence = qos_queue_dm_get_precedence(node->data.queue);
exit:
    return precedence;
}

uint32_t qos_node_queue_get_red_threshold(const qos_node_t* node) {
    uint32_t red_threshold = 0;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    red_threshold = qos_queue_dm_get_red_threshold(node->data.queue);
exit:
    return red_threshold;
}

uint32_t qos_node_queue_get_red_percentage(const qos_node_t* node) {
    uint32_t red_percentage = 0;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    red_percentage = qos_queue_dm_get_red_percentage(node->data.queue);
exit:
    return red_percentage;
}

qos_queue_drop_algorithm_t qos_node_queue_get_drop_algorithm(const qos_node_t* node) {
    qos_queue_drop_algorithm_t drop_algorithm = QOS_QUEUE_DROP_ALGORITHM_LAST;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    drop_algorithm = qos_queue_dm_get_drop_algorithm(node->data.queue);
exit:
    return drop_algorithm;
}

qos_scheduler_algorithm_t qos_node_queue_get_scheduler_algorithm(const qos_node_t* node) {
    qos_scheduler_algorithm_t scheduler_algorithm = QOS_SCHEDULER_ALGORITHM_LAST;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    scheduler_algorithm = qos_queue_dm_get_scheduler_algorithm(node->data.queue);
exit:
    return scheduler_algorithm;
}

int32_t qos_node_queue_get_shaping_rate(const qos_node_t* node) {
    int32_t shaping_rate = -1;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    shaping_rate = qos_queue_dm_get_shaping_rate(node->data.queue);
exit:
    return shaping_rate;
}

int32_t qos_node_queue_get_assured_rate(const qos_node_t* node) {
    int32_t assured_rate = -1;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    assured_rate = qos_queue_dm_get_assured_rate(node->data.queue);
exit:
    return assured_rate;
}

uint32_t qos_node_queue_get_queue_key(const qos_node_t* node) {
    uint32_t queue_key = 0;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    queue_key = qos_queue_dm_get_queue_key(node->data.queue);
exit:
    return queue_key;
}


const char* qos_node_queue_get_classkey_minor(const qos_node_t* node) {
    const char* classkey_minor = NULL;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    classkey_minor = qos_queue_dm_get_classkey_minor(node->data.queue);
exit:
    return classkey_minor;
}


uint32_t qos_node_queue_get_shaping_burst_size(const qos_node_t* node) {
    uint32_t shaping_burst_size = 0;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    shaping_burst_size = qos_queue_dm_get_shaping_burst_size(node->data.queue);
exit:
    return shaping_burst_size;
}

const char* qos_node_queue_get_queue_ctrl(const qos_node_t* node) {
    const char* queue_ctrl = NULL;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    queue_ctrl = qos_queue_dm_get_queue_ctrl(node->data.queue);
exit:
    return queue_ctrl;
}

uint32_t qos_node_queue_get_index(const qos_node_t* node) {
    uint32_t index = 0;

    when_null(node, exit);
    when_false(qos_node_is_queue(node), exit);

    index = qos_queue_dm_get_index(node->data.queue);
exit:
    return index;
}
