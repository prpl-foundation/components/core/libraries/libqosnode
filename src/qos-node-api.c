/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdint.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>

#include "qos-assert.h"
#include "qos-node-api.h"
#include "qos-node-api-priv.h"

static amxd_dm_t* _tr181_qos_dm;

int qos_node_api_init(amxd_dm_t* dm) {
    int retval = -1;

    when_null(dm, exit);
    when_not_null(_tr181_qos_dm, exit);

    _tr181_qos_dm = dm;
    retval = 0;

exit:
    return retval;
}

amxd_object_t* qos_node_get_object(const char* path) {
    amxd_object_t* object = NULL;

    when_str_empty(path, exit);
    when_null(_tr181_qos_dm, exit);

    object = amxd_dm_findf(_tr181_qos_dm, "%s", path);

exit:
    return object;
}

qos_node_t* qos_node_get_node(const char* path) {
    amxd_object_t* object = NULL;
    qos_node_t* node = NULL;

    when_str_empty(path, exit);
    when_null(_tr181_qos_dm, exit);

    when_false(strncmp(QOS_TR181_DEVICE_QOS_NODE_INSTANCE, path, strlen(QOS_TR181_DEVICE_QOS_NODE_INSTANCE)) == 0, exit);
    object = amxd_dm_findf(_tr181_qos_dm, "%s", path);

    when_null(object, exit);
    node = (qos_node_t*) object->priv;
exit:
    return node;
}

uint32_t qos_node_get_mark_mask(void) {
    return qos_dm_get_mark_mask();
}

bool qos_node_get_broken_qdisc_priomap(void) {
    return qos_dm_get_broken_qdisc_priomap();
}
