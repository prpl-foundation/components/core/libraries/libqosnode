/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <qoscommon/qos-shaper.h>
#include <qoscommon/qos-scheduler.h>
#include <qoscommon/qos-queue-stats.h>
#include <qoscommon/qos-queue.h>

#include "qos-node.h"
#include "qos-assert.h"
#include "qos-node-api-priv.h"

static int qos_node_find_root_nodes(const qos_node_t* const node, const qos_node_t* const previous_node, amxc_astack_t* stack);
static int qos_node_delete_descendants(qos_node_t* node);
static int qos_node_validate(const qos_node_t* const node);

static qos_node_type_t qos_node_get_type_from_path(const char* path) {
    when_str_empty(path, exit);

    if(!strncmp(path, QOS_TR181_DEVICE_QOS_QUEUE_INSTANCE, strlen(QOS_TR181_DEVICE_QOS_QUEUE_INSTANCE))) {
        return QOS_NODE_TYPE_QUEUE;
    }

    if(!strncmp(path, QOS_TR181_DEVICE_QOS_SCHEDULER_INSTANCE, strlen(QOS_TR181_DEVICE_QOS_SCHEDULER_INSTANCE))) {
        return QOS_NODE_TYPE_SCHEDULER;
    }

    if(!strncmp(path, QOS_TR181_DEVICE_QOS_SHAPER_INSTANCE, strlen(QOS_TR181_DEVICE_QOS_SHAPER_INSTANCE))) {
        return QOS_NODE_TYPE_SHAPER;
    }

    if(!strncmp(path, QOS_TR181_DEVICE_QOS_QUEUE_STATS_INSTANCE, strlen(QOS_TR181_DEVICE_QOS_QUEUE_STATS_INSTANCE))) {
        return QOS_NODE_TYPE_QUEUE_STATS;
    }

exit:
    return QOS_NODE_TYPE_NONE;
}

static inline void qos_node_set_type(qos_node_t* const node, const qos_node_type_t type) {
    if(node) {
        node->type = type;
    }
}

qos_node_type_t qos_node_get_type(const qos_node_t* const node) {
    qos_node_type_t type = QOS_NODE_TYPE_NONE;

    when_null(node, exit);
    type = node->type;

exit:
    return type;
}

static bool qos_node_llist_has_node(const amxc_llist_t* const list,
                                    const qos_node_t* const node) {
    bool retval = false;

    when_null(list, exit);
    when_null(node, exit);

    amxc_llist_for_each(it, list) {
        qos_node_t* ll_node = qos_node_llist_it_get_node(it);

        if(!ll_node) {
            continue;
        }

        if(ll_node == node) {
            retval = true;
            break;
        }
    }

exit:
    return retval;
}

static qos_node_t* qos_node_llist_get_by_type(const amxc_llist_t* const list,
                                              const qos_node_type_t type) {
    qos_node_t* node = NULL;

    when_null(list, exit);

    amxc_llist_for_each(it, list) {
        node = qos_node_llist_it_get_node(it);

        if(!node) {
            continue;
        }

        if(qos_node_get_type(node) == type) {
            return node;
        }
    }

exit:
    return NULL;
}

static int qos_node_llist_it_new(qos_node_llist_it_t** node_llist_it) {
    int retval = -1;

    when_null(node_llist_it, exit);
    when_not_null(*node_llist_it, exit);

    *node_llist_it = (qos_node_llist_it_t*) calloc(1, sizeof(qos_node_llist_it_t));
    when_null(*node_llist_it, exit);

    retval = 0;

exit:
    return retval;
}

static int qos_node_llist_it_delete(qos_node_llist_it_t** node_llist_it) {
    int retval = -1;

    when_null(node_llist_it, exit);
    when_null(*node_llist_it, exit);

    amxc_llist_it_take(&(*node_llist_it)->lit);
    free(*node_llist_it);
    *node_llist_it = NULL;

    retval = 0;

exit:
    return retval;
}

static void qos_node_llist_delete_it(amxc_llist_it_t* it) {
    qos_node_llist_it_t* node_llist_it = NULL;

    if(it) {
        node_llist_it = qos_node_llist_it(it);

        if(node_llist_it) {
            qos_node_llist_it_delete(&node_llist_it);
        }
    }
}

void qos_node_llist_delete(amxc_llist_t** llist) {
    if(!llist || !*llist) {
        return;
    }

    amxc_llist_delete(llist, qos_node_llist_delete_it);
}

static amxc_llist_t* qos_node_get_llist_by_type(const amxc_llist_t* const list,
                                                const qos_node_type_t type) {
    int retval = -1;
    amxc_llist_t* llist = NULL;
    qos_node_t* node = NULL;

    when_null(list, exit);
    when_true(amxc_llist_is_empty(list), exit);
    when_failed(amxc_llist_new(&llist), exit);

    amxc_llist_for_each(it, list) {
        node = qos_node_llist_it_get_node(it);

        if(!node) {
            continue;
        }

        if(qos_node_get_type(node) == type) {

            qos_node_llist_it_t* node_llist_it = NULL;

            retval = qos_node_llist_it_new(&node_llist_it);
            if(retval || !node_llist_it) {
                continue;
            }

            node_llist_it->node = node;
            amxc_llist_append(llist, &node_llist_it->lit);

        }
    }

    if(amxc_llist_is_empty(llist)) {
        qos_node_llist_delete(&llist);
    }

exit:
    return llist;
}

static int qos_node_llist_remove_node(const amxc_llist_t* const list,
                                      const qos_node_t* const node) {
    int retval = -1;

    when_null(list, exit);
    when_null(node, exit);

    amxc_llist_for_each(it, list) {
        qos_node_llist_it_t* node_llist_it = qos_node_llist_it(it);
        qos_node_t* ll_node = NULL;

        if(!node_llist_it) {
            continue;
        }

        ll_node = node_llist_it->node;

        if(!ll_node) {
            continue;
        }

        if(ll_node == node) {
            retval = qos_node_llist_it_delete(&node_llist_it);
            break;
        }
    }

exit:
    return retval;
}

static bool qos_node_has_parent(const qos_node_t* const node,
                                const qos_node_t* const parent) {
    bool retval = false;

    when_null(node, exit);
    when_null(parent, exit);
    when_true(node == parent, exit);

    retval = qos_node_llist_has_node(node->parents, parent);

exit:
    return retval;
}

static int qos_node_add_parent(qos_node_t* const node, qos_node_t* const parent) {
    int retval = -1;
    qos_node_llist_it_t* node_llist_it = NULL;

    when_null(node, exit);
    when_null(parent, exit);
    when_true(node == parent, exit);
    when_true_status(qos_node_has_parent(node, parent), exit, retval = 0);

    retval = qos_node_llist_it_new(&node_llist_it);
    when_failed(retval, exit);

    node_llist_it->node = parent;

    retval = amxc_llist_append(node->parents, &node_llist_it->lit);

exit:
    return retval;
}

static int qos_node_remove_parent(qos_node_t* const node, qos_node_t* const parent) {
    int retval = -1;

    when_null(node, exit);
    when_null(parent, exit);
    when_true(node == parent, exit);
    when_false_status(qos_node_has_parent(node, parent), exit, retval = 0);

    retval = qos_node_llist_remove_node(node->parents, parent);

exit:
    return retval;
}

static bool qos_node_has_child(const qos_node_t* const node,
                               const qos_node_t* const child) {
    bool retval = false;

    when_null(node, exit);
    when_null(child, exit);
    when_true(node == child, exit);

    retval = qos_node_llist_has_node(node->children, child);

exit:
    return retval;
}

static int qos_node_add_child(qos_node_t* const node, qos_node_t* const child) {
    int retval = -1;
    qos_node_llist_it_t* node_llist_it = NULL;

    when_null(node, exit);
    when_null(child, exit);
    when_true(node == child, exit);
    when_true_status(qos_node_has_child(node, child), exit, retval = 0);

    retval = qos_node_llist_it_new(&node_llist_it);
    when_failed(retval, exit);

    node_llist_it->node = child;

    retval = amxc_llist_append(node->children, &node_llist_it->lit);

exit:
    return retval;
}

static int qos_node_remove_child(qos_node_t* const node, qos_node_t* const child) {
    int retval = -1;

    when_null(node, exit);
    when_null(child, exit);
    when_true(node == child, exit);
    when_false_status(qos_node_has_child(node, child), exit, retval = 0);

    retval = qos_node_llist_remove_node(node->children, child);

exit:
    return retval;
}

static int qos_node_init_type_data(qos_node_t* const node) {
    int retval = -1;
    const char* node_reference_path = NULL;
    qos_node_type_t type = QOS_NODE_TYPE_NONE;
    amxd_object_t* reference_instance = NULL;

    when_null(node, exit);

    //QoS.Node.{i}.Reference
    node_reference_path = qos_node_dm_get_reference(node);
    when_str_empty(node_reference_path, exit);

    type = qos_node_get_type_from_path(node_reference_path);

    if(QOS_NODE_TYPE_NONE == type) {
        goto exit;
    }

    qos_node_set_type(node, type);

    reference_instance = qos_node_get_object(node_reference_path);
    when_null(reference_instance, exit);
    when_null(reference_instance->priv, exit);

    switch(node->type) {
    case QOS_NODE_TYPE_QUEUE:
        node->data.queue = (qos_queue_t*) reference_instance->priv;
        node->data.queue->node = node;
        break;
    case QOS_NODE_TYPE_SCHEDULER:
        node->data.scheduler = (qos_scheduler_t*) reference_instance->priv;
        node->data.scheduler->node = node;
        break;
    case QOS_NODE_TYPE_QUEUE_STATS:
        node->data.queue_stats = (qos_queue_stats_t*) reference_instance->priv;
        node->data.queue_stats->node = node;
        break;
    case QOS_NODE_TYPE_SHAPER:
        node->data.shaper = (qos_shaper_t*) reference_instance->priv;
        node->data.shaper->node = node;
        break;
    case QOS_NODE_TYPE_NONE:
        goto exit;
    }

    retval = 0;

exit:
    return retval;
}

static qos_node_t* qos_node_init_family_member(amxd_object_t* instance) {
    int retval = -1;
    qos_node_t* node = NULL;
    amxd_object_t* node_family_instance = NULL;
    const char* name = NULL;
    const char* abs_path_node_instance = QOS_TR181_DEVICE_QOS_NODE_INSTANCE;
    amxc_string_t name_path;
    bool append_abs_path = false;

    when_null(instance, exit);

    //QoS.Node.{i}.Parent|Child.Name
    name = amxc_var_constcast(cstring_t, amxd_object_get_param_value(instance, "Name"));
    when_str_empty(name, exit);

    append_abs_path = strncmp(name, abs_path_node_instance, strlen(abs_path_node_instance));

    retval = amxc_string_init(&name_path, 128);
    when_failed(retval, exit);

    retval = amxc_string_appendf(&name_path, "%s%s", (append_abs_path ? abs_path_node_instance : ""), name);
    when_failed(retval, exit);

    node_family_instance = qos_node_get_object(amxc_string_get(&name_path, 0));
    amxc_string_clean(&name_path);
    when_null(node_family_instance, exit);

    node = (qos_node_t*) node_family_instance->priv;

    if(!node) {
        retval = qos_node_new(&node, node_family_instance);
        when_failed(retval, exit);
    }

exit:
    return node;
}

static int qos_node_init_parent(qos_node_t* const node) {
    int retval = -1;
    amxd_object_t* parent_object = NULL;

    when_null(node, exit);

    //QoS.Node.{i}.Parent
    parent_object = amxd_object_get_child(qos_node_get_dm_object(node), "Parent");
    when_null_status(parent_object, exit, retval = 0);

    //QoS.Node.{i}.Parent.{j}
    amxd_object_for_each(instance, it, parent_object) {
        amxd_object_t* parent_instance = amxc_container_of(it, amxd_object_t, it);
        qos_node_t* node_parent = NULL;

        if(!parent_instance) {
            continue;
        }

        node_parent = qos_node_init_family_member(parent_instance);
        when_null(node_parent, exit);

        retval = qos_node_add_parent(node, node_parent);
        when_failed(retval, exit);

        retval = qos_node_add_child(node_parent, node);
        when_failed(retval, exit);
    }

    retval = 0;

exit:
    return retval;
}

static int qos_node_init_children(qos_node_t* const node) {
    int retval = -1;
    amxd_object_t* child_object = NULL;

    when_null(node, exit);

    //QoS.Node.{i}.Child
    child_object = amxd_object_get_child(qos_node_get_dm_object(node), "Child");
    when_null_status(child_object, exit, retval = 0);

    //QoS.Node.{i}.Child.{j}
    amxd_object_for_each(instance, it, child_object) {
        amxd_object_t* child_instance = amxc_container_of(it, amxd_object_t, it);
        qos_node_t* node_child = NULL;

        if(!child_instance) {
            continue;
        }

        node_child = qos_node_init_family_member(child_instance);
        when_null(node_child, exit);

        retval = qos_node_add_parent(node_child, node);
        when_failed(retval, exit);

        retval = qos_node_add_child(node, node_child);
        when_failed(retval, exit);
    }

    retval = 0;

exit:
    return retval;
}

amxd_object_t* qos_node_get_dm_object(const qos_node_t* const node) {
    amxd_object_t* object = NULL;

    when_null(node, exit);
    object = node->dm_object;

exit:
    return object;
}


int qos_node_dm_set_enable(const qos_node_t* const node, const bool enable) {
    int retval = -1;
    amxd_object_t* object = NULL;
    const char* param = "Enable";
    amxd_status_t status = amxd_status_ok;

    when_null(node, exit);
    object = qos_node_get_dm_object(node);
    when_null(object, exit);

    status = amxd_object_set_value(bool, object, param, enable);
    when_failed(status, exit);

    retval = 0;

exit:
    return retval;
}

qos_status_t qos_node_dm_get_status(const qos_node_t* const node) {
    qos_status_t status = QOS_STATUS_DISABLED;
    char* str_status = NULL;
    amxd_object_t* object = NULL;
    const char* param = "Status";

    when_null(node, exit);
    object = qos_node_get_dm_object(node);
    when_null(object, exit);

    str_status = amxd_object_get_value(cstring_t, object, param, NULL);
    when_str_empty(str_status, exit);

    status = qos_status_from_string(str_status);
    free(str_status);

exit:
    return status;
}

int qos_node_dm_set_status(const qos_node_t* const node, const qos_status_t status) {
    int retval = -1;
    const char* str_status = NULL;
    amxd_object_t* object = NULL;
    const char* param = "Status";
    amxd_status_t amx_status = amxd_status_ok;

    if(QOS_STATUS_LAST <= status) {
        goto exit;
    }

    when_null(node, exit);
    object = qos_node_get_dm_object(node);
    when_null(object, exit);

    str_status = qos_status_to_string(status);
    amx_status = amxd_object_set_value(cstring_t, object, param, str_status);
    when_failed(amx_status, exit);

    retval = 0;

exit:
    return retval;
}

int qos_node_new(qos_node_t** node, amxd_object_t* node_instance) {
    int retval = -1;

    when_null(node, exit);
    when_null(node_instance, exit);
    when_not_null(*node, exit);
    when_not_null(node_instance->priv, exit);

    *node = (qos_node_t*) calloc(1, sizeof(qos_node_t));
    when_null(*node, exit);

    retval = qos_node_init(*node, node_instance);
    when_failed(retval, failed);

exit:
    return retval;

failed:
    qos_node_delete(node);
    return retval;
}

int qos_node_delete(qos_node_t** node) {
    int retval = -1;
    qos_node_t* node_ptr = NULL;

    when_null(node, exit);
    when_null(*node, exit);

    node_ptr = *node;

    retval = qos_node_deinit(node_ptr);
    when_failed(retval, exit);

    free(node_ptr);
    *node = NULL;
    retval = 0;

exit:
    return retval;
}

static void qos_node_delete_child(amxc_llist_it_t* it) {
    qos_node_llist_it_t* node_llist_it = NULL;
    qos_node_t* node = NULL;

    if(it) {
        node_llist_it = qos_node_llist_it(it);

        if(node_llist_it) {
            node = node_llist_it->node;
            qos_node_delete_descendants(node);
            qos_node_llist_it_delete(&node_llist_it);
        }
    }
}

static int qos_node_delete_descendants(qos_node_t* node) {
    int retval = -1;

    when_null(node, exit);

    amxc_llist_clean(node->children, qos_node_delete_child);
    retval = qos_node_delete(&node);

exit:
    return retval;
}

static int qos_node_find_root_nodes(const qos_node_t* const node,
                                    const qos_node_t* const previous_node, amxc_astack_t* stack) {
    int retval = -1;
    amxc_astack_it_t* stack_it = NULL;

    when_null(node, exit);
    when_null(stack, exit);

    //Traverse up until no parent is found.
    if(amxc_llist_is_empty(node->parents)) {
        stack_it = amxc_astack_push(stack, (void*) node);
        when_null(stack_it, exit);
    } else {

        amxc_llist_for_each(it, node->parents) {
            qos_node_t* node_parent = qos_node_llist_it_get_node(it);

            if(!node_parent) {
                continue;
            }

            if(node_parent == previous_node) {
                continue;
            }

            retval = qos_node_find_root_nodes(node_parent, node, stack);
            when_failed(retval, exit);
        }
    }

    //Traverse down and skip checked nodes.
    amxc_llist_for_each(it, node->children) {
        qos_node_t* node_child = qos_node_llist_it_get_node(it);

        if(!node_child) {
            continue;
        }

        if(node_child == previous_node) {
            continue;
        }

        retval = qos_node_find_root_nodes(node_child, node, stack);
        when_failed(retval, exit);
    }

    retval = 0;

exit:
    return retval;
}

//Caller must free the stack via amxc_astack_delete.
static amxc_astack_t* qos_node_get_root_nodes(const qos_node_t* const node) {
    int retval = -1;
    amxc_astack_t* stack = NULL;

    when_null(node, exit);

    retval = amxc_astack_new(&stack);
    when_failed(retval, exit);

    retval = qos_node_find_root_nodes(node, NULL, stack);

    if(retval) {
        amxc_astack_delete(&stack, NULL);
    }

exit:
    return stack;
}

int qos_node_delete_tree(qos_node_t** node) {
    int retval = -1;
    qos_node_t* node_ptr = NULL;
    amxc_astack_t* stack_root_nodes = NULL;

    when_null(node, exit);
    when_null(*node, exit);

    node_ptr = *node;
    *node = NULL;

    stack_root_nodes = qos_node_get_root_nodes(node_ptr);
    when_null(stack_root_nodes, exit);

    while((node_ptr = (qos_node_t*) amxc_astack_pop(stack_root_nodes))) {
        retval = qos_node_delete_descendants(node_ptr);
        when_failed(retval, exit);
    }

exit:
    amxc_astack_delete(&stack_root_nodes, NULL);
    return retval;
}

static bool qos_node_llist_has_valid_type(const amxc_llist_t* const list,
                                          qos_node_type_t* const types, size_t len) {
    bool retval = false;

    when_null(list, exit);
    when_null(types, exit);
    when_true(0 == len, exit);
    when_true_status(amxc_llist_is_empty(list), exit, retval = true);

    amxc_llist_for_each(it, list) {
        qos_node_t* node = qos_node_llist_it_get_node(it);
        qos_node_type_t type = QOS_NODE_TYPE_NONE;
        size_t i;

        if(!node) {
            continue;
        }

        type = qos_node_get_type(node);

        for(i = 0; i < len; i++) {
            when_true_status(types[i] == type, exit, retval = true);
        }
    }

exit:
    return retval;
}

static int qos_node_validate_queue(const qos_node_t* const node) {
    int retval = -1;
    bool valid = false;
    qos_node_type_t parent_types[3];
    qos_node_type_t child_types[3];

    when_null(node, exit);
    when_true(qos_node_get_type(node) != QOS_NODE_TYPE_QUEUE, exit);

    parent_types[0] = QOS_NODE_TYPE_QUEUE;
    parent_types[1] = QOS_NODE_TYPE_SCHEDULER;
    parent_types[2] = QOS_NODE_TYPE_SHAPER;

    child_types[0] = QOS_NODE_TYPE_QUEUE;
    child_types[1] = QOS_NODE_TYPE_SCHEDULER;
    child_types[2] = QOS_NODE_TYPE_QUEUE_STATS;

    valid = qos_node_llist_has_valid_type(node->parents, parent_types, sizeof(parent_types) / sizeof(parent_types[0]));
    when_false(valid, exit);

    valid = qos_node_llist_has_valid_type(node->children, child_types, sizeof(child_types) / sizeof(child_types[0]));
    when_false(valid, exit);

    retval = 0;

exit:
    return retval;
}

static int qos_node_validate_shaper(const qos_node_t* const node) {
    int retval = -1;
    bool valid = false;
    qos_node_type_t parent_types[1];
    qos_node_type_t child_types[2];

    when_null(node, exit);
    when_true(qos_node_get_type(node) != QOS_NODE_TYPE_SHAPER, exit);

    parent_types[0] = QOS_NODE_TYPE_SCHEDULER;

    child_types[0] = QOS_NODE_TYPE_QUEUE;
    child_types[1] = QOS_NODE_TYPE_SCHEDULER;

    valid = qos_node_llist_has_valid_type(node->parents, parent_types, sizeof(parent_types) / sizeof(parent_types[0]));
    when_false(valid, exit);

    valid = qos_node_llist_has_valid_type(node->children, child_types, sizeof(child_types) / sizeof(child_types[0]));
    when_false(valid, exit);

    retval = 0;

exit:
    return retval;
}

static int qos_node_validate_scheduler(const qos_node_t* const node) {
    int retval = -1;
    bool valid = false;
    qos_node_type_t parent_types[2];
    qos_node_type_t child_types[3];

    when_null(node, exit);
    when_true(qos_node_get_type(node) != QOS_NODE_TYPE_SCHEDULER, exit);

    parent_types[0] = QOS_NODE_TYPE_SHAPER;
    parent_types[1] = QOS_NODE_TYPE_SCHEDULER;

    child_types[0] = QOS_NODE_TYPE_QUEUE;
    child_types[1] = QOS_NODE_TYPE_SCHEDULER;
    child_types[2] = QOS_NODE_TYPE_SHAPER;

    valid = qos_node_llist_has_valid_type(node->parents, parent_types, sizeof(parent_types) / sizeof(parent_types[0]));
    when_false(valid, exit);

    valid = qos_node_llist_has_valid_type(node->children, child_types, sizeof(child_types) / sizeof(child_types[0]));
    when_false(valid, exit);

    retval = 0;

exit:
    return retval;
}

static int qos_node_validate_queue_stats(const qos_node_t* const node) {
    int retval = -1;
    bool valid = false;
    qos_node_type_t parent_types[1];

    when_null(node, exit);
    when_true(qos_node_get_type(node) != QOS_NODE_TYPE_QUEUE_STATS, exit);
    when_true(amxc_llist_size(node->children) != 0, exit);

    parent_types[0] = QOS_NODE_TYPE_QUEUE;

    valid = qos_node_llist_has_valid_type(node->parents, parent_types, sizeof(parent_types) / sizeof(parent_types[0]));
    when_false(valid, exit);

    retval = 0;

exit:
    return retval;
}

static int qos_node_validate(const qos_node_t* const node) {
    int retval = -1;

    when_null(node, exit);

    switch(qos_node_get_type(node)) {
    case QOS_NODE_TYPE_NONE:
        break;
    case QOS_NODE_TYPE_QUEUE:
        retval = qos_node_validate_queue(node);
        break;
    case QOS_NODE_TYPE_QUEUE_STATS:
        retval = qos_node_validate_queue_stats(node);
        break;
    case QOS_NODE_TYPE_SCHEDULER:
        retval = qos_node_validate_scheduler(node);
        break;
    case QOS_NODE_TYPE_SHAPER:
        retval = qos_node_validate_shaper(node);
        break;
    }

exit:
    return retval;
}

int qos_node_init(qos_node_t* const node, amxd_object_t* const node_instance) {
    int retval = -1;

    when_null(node, exit);
    when_null(node_instance, exit);
    when_not_null(node_instance->priv, exit);

    memset(node, 0, sizeof(qos_node_t));

    retval = amxc_llist_new(&node->parents);
    when_failed(retval, exit);

    retval = amxc_llist_new(&node->children);
    when_failed(retval, exit);

    node->dm_object = node_instance;
    node_instance->priv = (void*) node;

    retval = qos_node_init_type_data(node);
    when_failed(retval, error_check);

    retval = qos_node_init_parent(node);
    when_failed(retval, error_check);

    retval = qos_node_init_children(node);
    when_failed(retval, error_check);

    retval = qos_node_validate(node);

error_check:
    if(retval) {
        qos_node_dm_set_status(node, QOS_STATUS_ERROR_MISCONFIGURED);
    }

exit:
    return retval;
}

static int qos_node_deinit_parents(qos_node_t* const node) {
    int retval = -1;

    when_null(node, exit);

    amxc_llist_for_each(it, node->parents) {
        qos_node_llist_it_t* node_llist_it = qos_node_llist_it(it);
        qos_node_t* node_parent = NULL;

        if(!node_llist_it) {
            continue;
        }

        node_parent = node_llist_it->node;

        if(!node_parent) {
            continue;
        }

        retval = qos_node_remove_child(node_parent, node);
        when_failed(retval, exit);

        retval = qos_node_llist_it_delete(&node_llist_it);
        when_failed(retval, exit);
    }

    amxc_llist_delete(&node->parents, NULL);
    retval = 0;

exit:
    return retval;
}

static int qos_node_deinit_children(qos_node_t* const node) {
    int retval = -1;

    when_null(node, exit);

    amxc_llist_for_each(it, node->children) {
        qos_node_llist_it_t* node_llist_it = qos_node_llist_it(it);
        qos_node_t* node_child = NULL;

        if(!node_llist_it) {
            continue;
        }

        node_child = node_llist_it->node;

        if(!node_child) {
            continue;
        }

        retval = qos_node_remove_parent(node_child, node);
        when_failed(retval, exit);

        retval = qos_node_llist_it_delete(&node_llist_it);
        when_failed(retval, exit);
    }

    amxc_llist_delete(&node->children, NULL);
    retval = 0;

exit:
    return retval;
}

int qos_node_deinit(qos_node_t* const node) {
    int retval = -1;

    when_null(node, exit);

    switch(node->type) {
    case QOS_NODE_TYPE_QUEUE:
        if(node->data.queue) {
            node->data.queue = NULL;
        }
        break;
    case QOS_NODE_TYPE_SCHEDULER:
        if(node->data.scheduler) {
            node->data.scheduler = NULL;
        }
        break;
    case QOS_NODE_TYPE_QUEUE_STATS:
        if(node->data.queue_stats) {
            node->data.queue_stats = NULL;
        }
        break;
    case QOS_NODE_TYPE_SHAPER:
        if(node->data.shaper) {
            node->data.shaper = NULL;
        }
        break;
    case QOS_NODE_TYPE_NONE:
        break;
    }

    retval = qos_node_deinit_parents(node);
    when_failed(retval, exit);

    retval = qos_node_deinit_children(node);
    when_failed(retval, exit);

    if(node->dm_object) {
        node->dm_object->priv = NULL;
        node->dm_object = NULL;
    }

    retval = 0;

exit:
    return retval;
}

qos_node_t* qos_node_find_parent_by_type(const qos_node_t* const node, const qos_node_type_t type) {
    qos_node_t* parent = NULL;

    when_null(node, exit);
    parent = qos_node_llist_get_by_type(node->parents, type);

exit:
    return parent;
}

qos_node_t* qos_node_find_child_by_type(const qos_node_t* const node, const qos_node_type_t type) {
    qos_node_t* child = NULL;

    when_null(node, exit);
    child = qos_node_llist_get_by_type(node->children, type);

exit:
    return child;
}

amxc_llist_t* qos_node_find_parents_by_type(const qos_node_t* const node, const qos_node_type_t type) {
    amxc_llist_t* list = NULL;

    when_null(node, exit);
    list = qos_node_get_llist_by_type(node->parents, type);

exit:
    return list;
}

amxc_llist_t* qos_node_find_children_by_type(const qos_node_t* const node, const qos_node_type_t type) {
    amxc_llist_t* list = NULL;

    when_null(node, exit);
    list = qos_node_get_llist_by_type(node->children, type);

exit:
    return list;
}

amxc_llist_t* qos_node_get_children(const qos_node_t* const node) {
    amxc_llist_t* children = NULL;

    when_null(node, exit);
    children = node->children;

exit:
    return children;
}

