/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__QOS_NODE_API_H__)
#define __QOS_NODE_API_H__

#ifdef __cplusplus
extern "C"
{
#endif

/**
   @file
   @brief
   QoS Node API header file
 */

/**
 * @mainpage
 * lib_qosnode is library containing functions that facilitates the use of the data-model representation of the object QoS.Node. provided by the tr181-qos plugins.
 * This library provides to the user a new type to manipulate explicit data rather than abstract amxd objects.
 * A node is the association two concepts, it reflects an object of the data-model wrapped by the library lib_qoscommon and establish hierarchy in the form of a bidirectional tree.
 * The hierarchy is created using the Children parameter of QoS objects.
 * In this way library provides functions to access the data reflected by the node in a read-only fashion and functions to traverse the hierarchy tree:
 *  - qos_node_t @ref qos_node_t
 *  - API specific functions @ref API
 *  - QoS data getters @ref Getters
 *  - Tree traversal @ref Hierarchy
 */

/**
 * @defgroup qos_node_t 1. Description of the qos_node_t type
 * @brief
 * Describe the node type provided by the library
 */

/**
 * @defgroup API 2. API specific functions
 * @brief
 * The list of functions provided by the API.
 */

/**
 * @defgroup Getters 3. QoS data getters
 * @brief
 * The list of functions allowing access to the QoS object reflected by the Node.
 */

/**
 * @defgroup Hierarchy 4. Hierarchy tree's traversal
 * @brief
 * The list of functions to traverse the hierarchy tree.
 */

#include <amxp/amxp.h>
#include <amxd/amxd_object.h>

#include "qos-node.h"

/**
 * @brief
 * Initialize the API with the provided data-model.
 * This function MUST be called before any call to the qos_node_get_node function
 * @param dm The data-model (for tr181-qos, the return of the function qos_get_dm).
 * @return int 0 on success
 * @return int -1 on failure
 * @ingroup API
 */
int qos_node_api_init(amxd_dm_t* dm);

/**
 * @brief
 * Get the qos_node_t object of a QoS.Node. instance.
 * @param path The data-model path of the object (e.g. QoS.Node.1.)
 * @return qos_node_t* the address on success
 * @return qos_node_t* NULL on failure
 * @ingroup API
 */
qos_node_t* qos_node_get_node(const char* path);

/**
 * @brief
 * Get the QoS.MarkMask value.
 * @ingroup API
 */
uint32_t qos_node_get_mark_mask(void);

/**
 * @brief
 * Get the QoS.BrokenQDiscPrioMap value.
 * @ingroup API
 */
bool qos_node_get_broken_qdisc_priomap(void);

//Queue getters functions

/**
 * @defgroup Queue 3.1. The Queue node type getters
 * @brief
 * @ingroup Getters
 */

/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the Enable parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return bool
 */
bool qos_node_queue_get_enable(const qos_node_t* node);
/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the Status parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return qos_status_t
 */
qos_status_t qos_node_queue_get_status(const qos_node_t* node);
/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the Alias parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return const char*
 */
const char* qos_node_queue_get_alias(const qos_node_t* node);
/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the TrafficClasses parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return const char*
 */
const char* qos_node_queue_get_traffic_classes(const qos_node_t* node);
/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the interface name
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return const char*
 */
const char* qos_node_queue_get_interface(const qos_node_t* node);
/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the AllInterfaces parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return bool
 */
bool qos_node_queue_get_all_interfaces(const qos_node_t* node);
/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the HardwareAssisted parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return bool
 */
bool qos_node_queue_get_hardware_assisted(const qos_node_t* node);
/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the BufferLength parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return uint32_t
 */
uint32_t qos_node_queue_get_buffer_length(const qos_node_t* node);
/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the Weight parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return uint32_t
 */
uint32_t qos_node_queue_get_weight(const qos_node_t* node);
/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the Precedence parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return uint32_t
 */
uint32_t qos_node_queue_get_precedence(const qos_node_t* node);
/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the REDThreshold parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return uint32_t
 */
uint32_t qos_node_queue_get_red_threshold(const qos_node_t* node);
/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the REDPercentage parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return uint32_t
 */
uint32_t qos_node_queue_get_red_percentage(const qos_node_t* node);
/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the DropAlgorithm parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return qos_queue_drop_algorithm_t
 */
qos_queue_drop_algorithm_t qos_node_queue_get_drop_algorithm(const qos_node_t* node);
/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the SchedulerAlgorithm parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return qos_scheduler_algorithm_t
 */
qos_scheduler_algorithm_t qos_node_queue_get_scheduler_algorithm(const qos_node_t* node);
/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the ShapingRate parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return int32_t
 */
int32_t qos_node_queue_get_shaping_rate(const qos_node_t* node);
/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the AssuredRate parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return int32_t
 */
int32_t qos_node_queue_get_assured_rate(const qos_node_t* node);
/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the QueueKey parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return uint32_t
 */
uint32_t qos_node_queue_get_queue_key(const qos_node_t* node);

/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the ClassKeyMinor parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return const char *
 */
const char* qos_node_queue_get_classkey_minor(const qos_node_t* node);

/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the ShapingBurstSize parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return uint32_t
 */
uint32_t qos_node_queue_get_shaping_burst_size(const qos_node_t* node);
/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the Controller parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return const char*
 */
const char* qos_node_queue_get_queue_ctrl(const qos_node_t* node);
/**
 * @ingroup Queue
 * @brief
 * If the node reflects a QoS.Queue object, return the instance's index of the QoS.Queue object (not the QoS.Node)
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE
 * @return uint32_t
 */
uint32_t qos_node_queue_get_index(const qos_node_t* node);

//QueueStats getters functions
/**
 * @defgroup QueueStats 3.2. The QueueStats node type getters
 * @brief
 * @ingroup Getters
 */

/**
 * @ingroup QueueStats
 * @brief
 * If the node reflects a QoS.QueueStats object, return the Enable parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE_STATS
 * @return bool
 */
bool qos_node_queuestats_get_enable(const qos_node_t* node);
/**
 * @ingroup QueueStats
 * @brief
 * If the node reflects a QoS.QueueStats object, return the Status parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE_STATS
 * @return qos_status_t
 */
qos_status_t qos_node_queuestats_get_status(const qos_node_t* node);
/**
 * @ingroup QueueStats
 * @brief
 * If the node reflects a QoS.QueueStats object, return the interface name
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE_STATS
 * @return const char*
 */
const char* qos_node_queuestats_get_interface(const qos_node_t* node);
/**
 * @ingroup QueueStats
 * @brief
 * If the node reflects a QoS.QueueStats object, return the Queue parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE_STATS
 * @return const char*
 */
const char* qos_node_queuestats_get_queue(const qos_node_t* node);

/**
 * @ingroup QueueStats
 * @brief
 * If the node reflects a QoS.QueueStats object, return the OutputPackets parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE_STATS
 * @return The number of packets sent.
 */
uint32_t qos_node_queuestats_get_output_packets(const qos_node_t* node);

/**
 * @ingroup QueueStats
 * @brief
 * If the node reflects a QoS.QueueStats object, return the OutputBytes parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE_STATS
 * @return The number of bytes sent.
 */
uint32_t qos_node_queuestats_get_output_bytes(const qos_node_t* node);

/**
 * @ingroup QueueStats
 * @brief
 * If the node reflects a QoS.QueueStats object, return the DroppedPackets parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE_STATS
 * @return The number of dropped packets.
 */
uint32_t qos_node_queuestats_get_dropped_packets(const qos_node_t* node);

/**
 * @ingroup QueueStats
 * @brief
 * If the node reflects a QoS.QueueStats object, return the DroppedBytes parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_QUEUE_STATS
 * @return The number of bytes dropped.
 */
uint32_t qos_node_queuestats_get_dropped_bytes(const qos_node_t* node);

//Scheduler getters functions
/**
 * @defgroup Scheduler 3.3. The Scheduler node type getters
 * @brief
 * @ingroup Getters
 */

/**
 * @ingroup Scheduler
 * @brief
 * If the node reflects a QoS.Scheduler object, return the Enable parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_SCHEDULER
 * @return bool
 */
bool qos_node_scheduler_get_enable(const qos_node_t* node);
/**
 * @ingroup Scheduler
 * @brief
 * If the node reflects a QoS.Scheduler object, return the Status parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_SCHEDULER
 * @return qos_status_t
 */
qos_status_t qos_node_scheduler_get_status(const qos_node_t* node);
/**
 * @ingroup Scheduler
 * @brief
 * If the node reflects a QoS.Scheduler object, return the Alias parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_SCHEDULER
 * @return const char*
 */
const char* qos_node_scheduler_get_alias(const qos_node_t* node);
/**
 * @ingroup Scheduler
 * @brief
 * If the node reflects a QoS.Scheduler object, return the interface name
 * @param node qos_node_t* of type QOS_NODE_TYPE_SCHEDULER
 * @return const char*
 */
const char* qos_node_scheduler_get_interface(const qos_node_t* node);
/**
 * @ingroup Scheduler
 * @brief
 * If the node reflects a QoS.Scheduler object, return the DefaultQueue parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_SCHEDULER
 * @return const char*
 */
const char* qos_node_scheduler_get_default_queue(const qos_node_t* node);
/**
 * @ingroup Scheduler
 * @brief
 * If the node reflects a QoS.Scheduler object, return the qos_node_t object that reflects the DefaultQueue parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_SCHEDULER
 * @return qos_node_t*
 */
qos_node_t* qos_node_scheduler_get_default_queue_node(const qos_node_t* node);
/**
 * @ingroup Scheduler
 * @brief
 * If the node reflects a QoS.Scheduler object, return the ShapingRate parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_SCHEDULER
 * @return int32_t
 */
int32_t qos_node_scheduler_get_shaping_rate(const qos_node_t* node);
/**
 * @ingroup Scheduler
 * @brief
 * If the node reflects a QoS.Scheduler object, return the AssuredRate parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_SCHEDULER
 * @return int32_t
 */
int32_t qos_node_scheduler_get_assured_rate(const qos_node_t* node);
/**
 * @ingroup Scheduler
 * @brief
 * If the node reflects a QoS.Scheduler object, return the SchedulerAlgorithm parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_SCHEDULER
 * @return qos_scheduler_algorithm_t
 */
qos_scheduler_algorithm_t qos_node_scheduler_get_scheduler_algorithm(const qos_node_t* node);

/**
 * @ingroup Scheduler
 * @brief
 * If the node reflects a QoS.Scheduler object, return the ClassKeyMajor parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_SCHEDULER
 * @return const char *
 */
const char* qos_node_scheduler_get_classkey_major(const qos_node_t* node);

/**
 * @ingroup Scheduler
 * @brief
 * If the node reflects a QoS.Scheduler object, return the instance's index of the QoS.Scheduler object (not the QoS.Node)
 * @param node qos_node_t* of type QOS_NODE_TYPE_SCHEDULER
 * @return uint32_t
 */
uint32_t qos_node_scheduler_get_index(const qos_node_t* node);

//Shaper getters functions
/**
 * @defgroup Shaper 3.4. The Shaper node type getters
 * @brief
 * @ingroup Getters
 */

/**
 * @ingroup Shaper
 * @brief
 * If the node reflects a QoS.Shaper object, return the Enable parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_SHAPER
 * @return bool
 */
bool qos_node_shaper_get_enable(const qos_node_t* node);
/**
 * @ingroup Shaper
 * @brief
 * If the node reflects a QoS.Shaper object, return the Status parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_SHAPER
 * @return qos_status_t
 */
qos_status_t qos_node_shaper_get_status(const qos_node_t* node);
/**
 * @ingroup Shaper
 * @brief
 * If the node reflects a QoS.Shaper object, return the interface name
 * @param node qos_node_t* of type QOS_NODE_TYPE_SHAPER
 * @return const char*
 */
const char* qos_node_shaper_get_interface(const qos_node_t* node);
/**
 * @ingroup Shaper
 * @brief
 * If the node reflects a QoS.Shaper object, return the ShapingRate parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_SHAPER
 * @return int32_t
 */
int32_t qos_node_shaper_get_shaping_rate(const qos_node_t* node);
/**
 * @ingroup Shaper
 * @brief
 * If the node reflects a QoS.Shaper object, return the ShapingBurstSize parameter
 * @param node qos_node_t* of type QOS_NODE_TYPE_SHAPER
 * @return uint32_t
 */
uint32_t qos_node_shaper_get_shaping_burst_size(const qos_node_t* node);
/**
 * @ingroup Shaper
 * @brief
 * If the node reflects a QoS.Shaper object, return the instance's index of the QoS.Shaper object (not the QoS.Node)
 * @param node qos_node_t* of type QOS_NODE_TYPE_SHAPER
 * @return uint32_t
 */
uint32_t qos_node_shaper_get_index(const qos_node_t* node);

#ifdef __cplusplus
}
#endif

#endif // __QOS_NODE_API_H__
