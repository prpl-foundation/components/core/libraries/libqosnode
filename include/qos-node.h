/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__QOS_NODE_H__)
#define __QOS_NODE_H__

#ifdef __cplusplus
extern "C"
{
#endif

/**
   @file
   @brief
   QoS Node type header file
 */

#include <stdint.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <qoscommon/qos-type.h>

#define QOS_TR181_DEVICE_QOS_NODE_PATH "QoS.Node"
#define QOS_TR181_DEVICE_QOS_NODE_INSTANCE "QoS.Node."
#define QOS_TR181_DEVICE_QOS_NODE_OBJECT_NAME "Node"

struct _qos_queue;
struct _qos_scheduler;
struct _qos_shaper;
struct _qos_queue_stats;

/**
 * @brief
 * Enumeration for the type of object reflected by the node.
 * @ingroup qos_node_t
 * @enum qos_node_type_t
 */
typedef enum _qos_node_type {
    QOS_NODE_TYPE_NONE,        ///< Default value
    QOS_NODE_TYPE_QUEUE,       ///< The reflected object is a Queue
    QOS_NODE_TYPE_QUEUE_STATS, ///< The reflected object is a QueueStats
    QOS_NODE_TYPE_SCHEDULER,   ///< The reflected object is a Scheduler
    QOS_NODE_TYPE_SHAPER       ///< The reflected object is a Shaper
} qos_node_type_t;

/**
 * @brief
 * The structure qos_node_t.
 * @struct qos_node_t
 * @ingroup qos_node_t
 */
typedef struct _qos_node {
    amxc_llist_t* parents;    /**< List of parent nodes */
    amxc_llist_t* children;   /**< List of child nodes */
    amxd_object_t* dm_object; /**< The data model object */
    qos_node_type_t type;     /**< The type of the node */
    union {
        struct _qos_queue* queue;
        struct _qos_scheduler* scheduler;
        struct _qos_shaper* shaper;
        struct _qos_queue_stats* queue_stats;
    } data; /**< A pointer to the underlying object */
} qos_node_t;

/**
 * @brief
 * The iterator for the qos_node_t hierarchy
 * @struct qos_node_llist_it_t
 * @ingroup Hierarchy
 */
typedef struct _qos_node_llist_it {
    amxc_llist_it_t lit;    ///< amxc iterator
    struct _qos_node* node; ///< qos_node_t data
} qos_node_llist_it_t;

#define qos_node_llist_it(it) \
    ((qos_node_llist_it_t*) (amxc_llist_it_get_data(it, qos_node_llist_it_t, lit)))

#define qos_node_llist_it_get_node(it) \
    (((qos_node_llist_it_t*) (amxc_llist_it_get_data(it, qos_node_llist_it_t, lit)))->node)

/**
 * @brief
 * Return the amxd_object_t used by the data-model.
 * Breaks the encapsulation, use carefully.
 * @ingroup qos_node_t
 * @param node
 * @return amxd_object_t*
 */
amxd_object_t* qos_node_get_dm_object(const qos_node_t* const node);

/**
 * @brief
 * Change the status of the Qos.Node. instance.
 * @ingroup qos_node_t
 * @param node
 * @param status
 * @return int 0 on success
 * @return int -1 on failure
 */
int qos_node_dm_set_status(const qos_node_t* const node, const qos_status_t status);
/**
 * @brief
 * Get the status of the QoS.Node. instance.
 * @ingroup qos_node_t
 * @param node
 * @return qos_status_t
 */
qos_status_t qos_node_dm_get_status(const qos_node_t* const node);
/**
 * @brief
 * Change the availability of the QoS.Node. instance.
 * @ingroup qos_node_t
 * @param node
 * @param enable
 * @return int
 */
int qos_node_dm_set_enable(const qos_node_t* const node, const bool enable);
/**
 * @brief
 * Get the availability of the QoS.Node. instance.
 * @ingroup qos_node_t
 * @param node
 * @return true
 * @return false
 */
static inline bool qos_node_dm_get_enable(const qos_node_t* const node) {
    return node == NULL ? false : amxd_object_get_value(bool, node->dm_object, "Enable", NULL);
}
/**
 * @brief
 * Get the alias of the QoS.Node. instance.
 * @ingroup qos_node_t
 * @param node
 * @return const char*
 */
static inline const char* qos_node_dm_get_alias(const qos_node_t* const node) {
    return node == NULL ? NULL : amxc_var_constcast(cstring_t, amxd_object_get_param_value(node->dm_object, "Alias"));
}
/**
 * @brief
 * Get the path of the QoS object the node reflects on.
 * @ingroup qos_node_t
 * @param node
 * @return const char*
 */
static inline const char* qos_node_dm_get_reference(const qos_node_t* const node) {
    return node == NULL ? NULL : amxc_var_constcast(cstring_t, amxd_object_get_param_value(node->dm_object, "Reference"));
}
/**
 * @brief
 * Get the parent path of the QoS.Node. instance.
 * @ingroup qos_node_t
 * @param node
 * @return const char*
 */
static inline const char* qos_node_dm_get_parent(const qos_node_t* const node) {
    return node == NULL ? NULL : amxc_var_constcast(cstring_t, amxd_object_get_param_value(node->dm_object, "Parent"));
}

/**
 * @brief
 * Allocate a new qos_node_t object and initialize is value with x_node_instance.
 * This function doesn't add a node in the data-model
 * @ingroup qos_node_t
 * @param node
 * @param x_node_instance
 * @return int 0 on success
 * @return int -1 on failure
 */
int qos_node_new(qos_node_t** node, amxd_object_t* const x_node_instance);
/**
 * @brief
 * Deallocate a qos_node_t object and set its pointer to NULL.
 * This function delete only the node passed in argument.
 * This function doesn't delete a node in the data-model.
 * @ingroup qos_node_t
 * @param node
 * @return int 0 on success
 * @return int -1 on failure
 */
int qos_node_delete(qos_node_t** node);
/**
 * @brief
 * Find the root node of the hierarchy the node belongs to. Deletes it with its children recursively.
 * This function doesn't delete nodes in the data-model.
 * @ingroup qos_node_t
 * @param node
 * @return int 0 on success
 * @return int -1 on failure
 */
int qos_node_delete_tree(qos_node_t** node);
/**
 * @brief
 * Initialize a qos_node_t previously allocated with x_node_instance.
 * @ingroup qos_node_t
 * @param node
 * @param x_node_instance
 * @return int 0 on success
 * @return int -1 on failure
 */
int qos_node_init(qos_node_t* const node, amxd_object_t* const x_node_instance);
/**
 * @brief
 * Deinitialize a qos_node_t.
 * @ingroup qos_node_t
 * @param node
 * @return int 0 on success
 * @return int -1 on failure
 */
int qos_node_deinit(qos_node_t* const node);

/**
 * @brief
 * Return the qos_node_t object of the node's direct parent of the specified type.
 * If the node has multiple parents it returns the first.
 * @ingroup Hierarchy
 * @param node
 * @param type
 * @return qos_node_t*
 */
qos_node_t* qos_node_find_parent_by_type(const qos_node_t* const node, const qos_node_type_t type);
/**
 * @brief
 * Return the qos_node_t object of the node's direct child of the specified type.
 * If the node has multiple children it returns the first found.
 * @ingroup Hierarchy
 * @param type
 * @return qos_node_t*
 */
qos_node_t* qos_node_find_child_by_type(const qos_node_t* const node, const qos_node_type_t type);

/**
 * @brief
 * Return a list of the node's direct parents of the specified type.
 *
 * Caller must free the list via qos_node_llist_delete.
 * @see qos_node_llist_it_t
 * @ingroup Hierarchy
 * @param node
 * @param type
 * @return amxc_llist_t*
 */
amxc_llist_t* qos_node_find_parents_by_type(const qos_node_t* const node, const qos_node_type_t type);
/**
 * @brief
 * Return a list of the node's direct children of the specified type.
 *
 * Caller must free the list via qos_node_llist_delete.
 * @see qos_node_llist_it_t
 * @ingroup Hierarchy
 * @param node
 * @param type
 * @return amxc_llist_t*
 */
amxc_llist_t* qos_node_find_children_by_type(const qos_node_t* const node, const qos_node_type_t type);
/**
 * @brief
 * Free the list previously allocated by qos_node_find_parents_by_type or qos_node_find_children_by_type.
 * @ingroup Hierarchy
 * @param list
 */
void qos_node_llist_delete(amxc_llist_t** list);
/**
 * @brief
 * Return a list of the node's direct children.
 *
 * @see qos_node_llist_it_t
 * @ingroup Hierarchy
 * @param node
 * @return amxc_llist_t*
 */
amxc_llist_t* qos_node_get_children(const qos_node_t* const node);
/**
 * @brief
 * Return the type of a node
 *
 * @param node
 * @return qos_node_type_t
 */
qos_node_type_t qos_node_get_type(const qos_node_t* const node);
#ifdef __cplusplus
}
#endif

#endif // __QOS_NODE_H__
